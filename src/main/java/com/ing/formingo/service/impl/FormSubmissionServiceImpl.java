package com.ing.formingo.service.impl;

import com.ing.formingo.entity.FormSubmission;
import com.ing.formingo.repository.FormSubmissionRepository;
import com.ing.formingo.service.FormSubmissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FormSubmissionServiceImpl extends BaseServiceImpl<FormSubmission> implements FormSubmissionService {

    private FormSubmissionRepository formSubmissionRepository;

    @Autowired
    public FormSubmissionServiceImpl(FormSubmissionRepository formSubmissionRepository) {
        super(formSubmissionRepository);
        this.formSubmissionRepository = formSubmissionRepository;
    }

    @Override
    public Optional<FormSubmission> getByUuidKey(String uuid) {
        return formSubmissionRepository.findByUuidKey(uuid);
    }

    @Override
    public Optional<Page<FormSubmission>> getAllByForm_IdOrderByCreatedDesc(Integer formId, Pageable pageable) {
        return formSubmissionRepository.findAllByForm_IdOrderByCreatedDesc(formId, pageable);
    }
}
