package com.ing.formingo.service.impl;

import com.ing.formingo.entity.Form;
import com.ing.formingo.repository.FormRepository;
import com.ing.formingo.service.FormService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FormServiceImpl extends BaseServiceImpl<Form> implements FormService {

    private FormRepository formRepository;
    private ModelMapper modelMapper;

    @Autowired
    public FormServiceImpl(FormRepository formRepository, ModelMapper modelMapper) {
        super(formRepository);
        this.formRepository = formRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public Optional<Form> getByUuidKey(String uuid) {
        return formRepository.findByUuidKey(uuid);
    }

    @Override
    public Optional<Form> getByHtmlName(String name) {
        return formRepository.findByHtmlName(name);
    }

    @Override
    public Optional<Page<Form>> getAllByWebsiteUuidKey(String uuid, Pageable pageable) {
        return formRepository.findAllByWebsiteUuidKey(uuid, pageable);
    }

    @Override
    public Optional<Page<Form>> getAllByWebsiteId(Integer id, Pageable pageable) {
        return formRepository.findAllByWebsiteId(id, pageable);
    }

    public void softDelete(Integer id) {
        Optional<Form> optional = formRepository.findById(id);
        if (optional.isPresent()) {
            Form formToSoftDelete = modelMapper.map(optional.get(), Form.class);
            formToSoftDelete.setIsActive(false);
            formRepository.save(formToSoftDelete);
        }
    }

    @Override
    public void softDeleteAllBySiteId(Integer id) {
        List<Form> formLisToSoftDelete = formRepository.findAllByWebsiteId(id);
        formLisToSoftDelete.forEach(form -> form.setIsActive(false));
    }
}
