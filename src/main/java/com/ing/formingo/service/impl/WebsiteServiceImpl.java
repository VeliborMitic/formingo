package com.ing.formingo.service.impl;

import com.ing.formingo.entity.Website;
import com.ing.formingo.repository.WebsiteRepository;
import com.ing.formingo.service.WebsiteService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class WebsiteServiceImpl extends BaseServiceImpl<Website> implements WebsiteService {
    private WebsiteRepository websiteRepository;
    private ModelMapper modelMapper;

    @Autowired
    public WebsiteServiceImpl(WebsiteRepository websiteRepository, ModelMapper modelMapper) {
        super(websiteRepository);
        this.websiteRepository = websiteRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public Optional<Website> getByUuidKey(String uuid) {
        return websiteRepository.findByUuidKey(uuid);
    }

    @Override
    public Optional<Website> getByDomain(String domain) {
        return websiteRepository.findByDomain(domain);
    }

    @Override
    public Boolean existsByDomain(String domain) {
        return websiteRepository.existsByDomain(domain);
    }

    @Override
    public Boolean existsByDomainAndDifferentId(String domain, Integer myId) {
        return websiteRepository.existsByDomainAndDifferentId(domain, myId);
    }

    @Override
    public Optional<Page<Website>> getAllByUserUuidKeyAndIsActiveTrue(
            String userUuid, Pageable pageable) {
        return websiteRepository.findAllByUserUuidKeyAndIsActiveTrueOrderByDomainNameAscDomainAsc(userUuid, pageable);
    }

    public void softDelete(Integer id) {
        Optional<Website> optional = websiteRepository.findById(id);
        if (optional.isPresent()) {
            Website siteToSoftDelete = modelMapper.map(optional.get(), Website.class);
            siteToSoftDelete.setIsActive(false);
            websiteRepository.save(siteToSoftDelete);
        }
    }
}