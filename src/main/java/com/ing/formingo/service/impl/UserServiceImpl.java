package com.ing.formingo.service.impl;

import com.ing.formingo.entity.User;
import com.ing.formingo.repository.UserRepository;
import com.ing.formingo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {
    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public Optional<User> getByUuidKey(String uuid) {
        return userRepository.findByUuidKey(uuid);
    }
}
