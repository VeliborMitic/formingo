package com.ing.formingo.service.impl;

import com.ing.formingo.entity.BaseEntity;
import com.ing.formingo.entity.Role;
import com.ing.formingo.repository.BaseRepository;
import com.ing.formingo.repository.filtering.SpecificationBuilder;
import com.ing.formingo.service.BaseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class BaseServiceImpl<T extends BaseEntity> implements BaseService<T> {

    private BaseRepository<T> baseRepository;

    public BaseServiceImpl(BaseRepository<T> baseRepository) {
        this.baseRepository = baseRepository;
    }

    @Override
    public List<T> get() {
        return baseRepository.findAll();
    }

    @Override
    public List<T> get(Sort sort) {
        return baseRepository.findAll(sort);
    }

    @Override
    public Optional<Page<T>> get(Pageable pageable) {
        return Optional.of(baseRepository.findAll(pageable));
    }

    @Override
    public Optional<T> get(Integer id) {
        return baseRepository.findById(id);
    }

    @Override
    public Optional<T> create(T entity) {
        return Optional.of(baseRepository.save(entity));
    }

    @Override
    public Optional<T> update(T entity) {
        if (get(entity.getId()).isPresent()) {
            return Optional.of(baseRepository.save(entity));
        }

        return Optional.empty();
    }

    @Override
    public void delete(Integer id) {
        baseRepository.deleteById(id);
    }

    public void delete(T entity){baseRepository.delete(entity);}

    public List<T> get(String search, Sort sort) {
        Specification<T> spec = buildSpecification(search);
        return baseRepository.findAll(spec, sort);
    }

    public Optional<Page<T>> get(String search, Pageable pageable) {
        Specification<T> spec = buildSpecification(search);
        return Optional.of(baseRepository.findAll(spec, pageable));
    }


    private Specification<T> buildSpecification(String searchRequest) {
        try {
            Pattern requestPattern = Pattern.compile("(\\w+)([~<>])([\\w]\\S*?),");
            Matcher requestMatcher = requestPattern.matcher(searchRequest + ",");

            SpecificationBuilder builder = new SpecificationBuilder();

            while (requestMatcher.find()) {
                String key = requestMatcher.group(1);
                String operator = requestMatcher.group(2);
                String value = requestMatcher.group(3);

                validateSearchTerm(key);

                if (key.equals("created") || key.equals("updated") || key.equals("dateTime")) {
                    builder.with(key, operator, parseInstant(value));
                } else if (key.equals("applicantStatus")) {
                    builder.with(key, operator, parseEnum(value));
                } else {
                    builder.with(key, operator, value);
                }
            }

            Specification spec = builder.build();

            if (spec == null) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Operator invalid. Please use '~' for equals, " +
                        "'>' for greater than or equal to, or '<' for less than or equal to. Please make sure that the " +
                        "operation you are trying to launch makes sense. 'email>asd' is not acceptable, whilst 'email~asd' is.");
            }

            return spec;
        } catch (IllegalStateException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Please enter valid search term, operator and/or value.");
        }
    }

    private Enum<Role> parseEnum(String role) {
        try {
            return Role.valueOf(role.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Please enter valid role!");
        }
    }

    private Instant parseInstant(String instantString) {
        try {
            return Instant.parse(instantString);
        } catch (DateTimeParseException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Please enter a valid date-time value using " +
                    "ISO-8601 representation, e.g. '2019-01-01T23:59:59Z'.");
        }
    }

    private void validateSearchTerm(String search) {
        List<String> validSearchTerms = Arrays.asList("id", "created", "updated", "username", "email", "role",
                "domain", "websiteName", "totalSubmissions", "htmlName");

        if (!validSearchTerms.contains(search)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Search term invalid. Please use 'id', 'created', 'updated', 'username', 'email', 'role', " +
                            "'domain', 'websiteName', 'totalSubmissions' or 'htmlName'");
        }
    }
}