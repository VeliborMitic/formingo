package com.ing.formingo.service.impl;

import com.ing.formingo.entity.Notification;
import com.ing.formingo.repository.NotificationRepository;
import com.ing.formingo.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class NotificationServiceImpl extends BaseServiceImpl<Notification> implements NotificationService {

    private NotificationRepository notificationRepository;

    @Autowired
    public NotificationServiceImpl(NotificationRepository notificationRepository) {
        super(notificationRepository);
        this.notificationRepository = notificationRepository;
    }

    @Override
    public Optional<Notification> getByUuidKey(String uuid) {
        return notificationRepository.findByUuidKey(uuid);
    }

    @Override
    public Optional<Notification> getByFormHtmlName(String name) {
        return notificationRepository.findByFormHtmlName(name);
    }

    @Override
    public Optional<Page<Notification>> getAllByFormIdOrderByCreatedDesc(Integer id, Pageable pageable) {
        return notificationRepository.findAllByFormIdOrderByCreatedDesc(id, pageable);
    }

    @Override
    public List<Notification> getAllByFormHtmlName(String formHtmlName) {
        return notificationRepository.findAllByFormHtmlName(formHtmlName);
    }
}
