package com.ing.formingo.service;

import com.ing.formingo.entity.Notification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface NotificationService extends BaseService<Notification> {

    Optional<Notification> getByFormHtmlName(String name);

    Optional<Page<Notification>> getAllByFormIdOrderByCreatedDesc(Integer id, Pageable pageable);

    List<Notification> getAllByFormHtmlName(String formHtmlName);
}