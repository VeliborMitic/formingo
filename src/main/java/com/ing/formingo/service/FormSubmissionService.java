package com.ing.formingo.service;

import com.ing.formingo.entity.FormSubmission;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface FormSubmissionService extends BaseService<FormSubmission> {

    Optional<Page<FormSubmission>> getAllByForm_IdOrderByCreatedDesc(Integer formId, Pageable pageable);
}