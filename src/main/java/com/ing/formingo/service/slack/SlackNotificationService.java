package com.ing.formingo.service.slack;

import com.github.seratch.jslack.Slack;
import com.github.seratch.jslack.api.webhook.Payload;
import com.github.seratch.jslack.api.webhook.WebhookResponse;
import com.ing.formingo.dto.slack.SlackMessageModel;
import com.ing.formingo.entity.Notification;
import com.ing.formingo.service.NotificationService;
import com.ing.formingo.service.WebsiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SlackNotificationService {
    private NotificationService notificationService;
    private WebsiteService websiteService;

    @Autowired
    public SlackNotificationService(NotificationService notificationService, WebsiteService websiteService) {
        this.notificationService = notificationService;
        this.websiteService = websiteService;
    }

    public void sendSlackNotification(SlackMessageModel model) throws IOException {
        model.setWebsiteName(websiteService.getByUuidKey(model.getSiteUuidKey()).get().getDomain());

        List<Notification> notificationList = notificationService.getAllByFormHtmlName(model.getFormHtmlName());

        List<String> urlToNotify = notificationList.stream()
                .filter(notification -> !notification.getSlackWebHook().isEmpty())
                .map(Notification::getSlackWebHook).collect(Collectors.toList());

        String message = "New submission for \"" + model.getFormHtmlName() + "\" on \"" + model.getWebsiteName() + "\"\n" +
                "Name: " + "" + model.getName() + ",\n" +
                "Email: " + "" + model.getEmail() + ",\n" +
                "Message: " + model.getMessage() + ",\n" +
                "Submitted on: " + model.getPrettyCreated();

        Payload payload = Payload.builder()
                .text(message)
                .build();

        for (String url : urlToNotify) {
            if (url != null) {
                Slack slack = Slack.getInstance();
                WebhookResponse response = slack.send(url, payload);
            }
        }
    }
}
