package com.ing.formingo.service;

import com.ing.formingo.entity.User;

public interface UserService extends BaseService<User> {

    User findByEmail(String email);
}