package com.ing.formingo.service.mail;

import com.ing.formingo.dto.email.EmailModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
public class MailContentBuilder {

    private TemplateEngine templateEngine;

    @Autowired
    public MailContentBuilder(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    //    String build(Map<String, Object> model) {
    String build(EmailModel model) {
        Context context = new Context();
        context.setVariable("model", model);
        return templateEngine.process("email/mailTemplate", context);
    }
}