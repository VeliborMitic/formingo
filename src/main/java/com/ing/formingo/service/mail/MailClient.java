package com.ing.formingo.service.mail;

import com.ing.formingo.entity.Notification;
import com.ing.formingo.dto.email.EmailModel;
import com.ing.formingo.service.NotificationService;
import com.ing.formingo.service.WebsiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;

@Service
public class MailClient {

    private JavaMailSender mailSender;
    private MailContentBuilder mailContentBuilder;
    private NotificationService notificationService;
    private WebsiteService websiteService;

    @Autowired
    public MailClient(JavaMailSender mailSender,
                      MailContentBuilder mailContentBuilder,
                      NotificationService notificationService,
                      WebsiteService websiteService) {
        this.mailSender = mailSender;
        this.mailContentBuilder = mailContentBuilder;
        this.notificationService = notificationService;
        this.websiteService = websiteService;
    }

    public void prepareAndSend(EmailModel emailModel) {
        emailModel.setWebsiteName(websiteService.getByUuidKey(emailModel.getSiteUuidKey()).get().getDomain());

        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);

            messageHelper.setFrom("formingo@ingsoftware.tech");
            messageHelper.setSentDate(Date.from(Instant.now()));
            messageHelper.setTo(notificationService.getAllByFormHtmlName(emailModel.getFormHtmlName())
                    .stream()
                    .map(Notification::getEmail).toArray(String[]::new));

            messageHelper.setSubject("New Form Submission For "
                    + emailModel.getFormHtmlName() + " On "
                    + emailModel.getWebsiteName());

            String content = mailContentBuilder.build(emailModel);
            messageHelper.setText(content, true);
        };
        try {
            mailSender.send(messagePreparator);
        } catch (MailException e) {
            e.getMessage();
        }
    }
}
