package com.ing.formingo.service;

import com.ing.formingo.entity.Website;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface WebsiteService extends BaseService<Website> {

    Optional<Website> getByDomain(String domain);

    Boolean existsByDomain(String domain);

    Boolean existsByDomainAndDifferentId(String domain, Integer myId);

    Optional<Page<Website>> getAllByUserUuidKeyAndIsActiveTrue(String userUuid, Pageable pageable);

    void softDelete(Integer id);

    //TODO: Remove this if not necessary
    //Optional<Page<Website>> findAllByUserUuidKey(String userUuid, Pageable pageable);
}