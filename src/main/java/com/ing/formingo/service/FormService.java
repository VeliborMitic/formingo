package com.ing.formingo.service;

import com.ing.formingo.entity.Form;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface FormService extends BaseService<Form> {

    Optional<Form> getByHtmlName(String name);

    Optional<Page<Form>> getAllByWebsiteUuidKey(String uuid, Pageable pageable);

    Optional<Page<Form>> getAllByWebsiteId(Integer id, Pageable pageable);

    void softDelete(Integer id);

    void softDeleteAllBySiteId(Integer id);
}