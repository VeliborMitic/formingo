package com.ing.formingo.service.security;

import com.ing.formingo.entity.User;
import com.ing.formingo.dto.user.LoggedUser;
import com.ing.formingo.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class AuthService {
    private ModelMapper modelMapper;
    private UserService userService;

    public AuthService(ModelMapper modelMapper, UserService userService) {
        this.modelMapper = modelMapper;
        this.userService = userService;
    }

    public LoggedUser getLoggedUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByEmail(auth.getName());
        return modelMapper.map(user, LoggedUser.class);
    }
}
