package com.ing.formingo.service.validation.website;

import com.ing.formingo.entity.Website;
import com.ing.formingo.service.WebsiteService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

public class UniqueDomainNameValidator implements ConstraintValidator<UniqueDomainName, String> {
    private WebsiteService websiteService;

    @Autowired
    public UniqueDomainNameValidator(WebsiteService websiteService) {
        this.websiteService = websiteService;
    }

    @Override
    public boolean isValid(String domain, ConstraintValidatorContext constraintValidatorContext) {
        Optional<Website> optionalWebsite = websiteService.getByDomain(domain);
        return optionalWebsite.map(website ->
                !websiteService.existsByDomainAndDifferentId(domain, website.getId()))
                .orElseGet(() -> !websiteService.existsByDomain(domain));
    }
}