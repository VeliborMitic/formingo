package com.ing.formingo.service.validation.website;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NoDirtyWordsValidator implements ConstraintValidator<NoDirtyWords, String> {
    private static final Logger LOGGER = LoggerFactory.getLogger(NoDirtyWordsValidator.class);

    @Value("${validation.file.location}")
    private String validationFile;

    @Override
    public boolean isValid(String sentenceToValidate, ConstraintValidatorContext constraintValidatorContext) {

        List<String> wordsToValidate = Arrays.asList(sentenceToValidate.toLowerCase().split("\\s+"));

        Stream<String> lines = null;
        try {
            lines = Files.lines(Paths.get(validationFile));
        } catch (IOException e) {
            LOGGER.error("File not found!", e);
        }
        List<String> dirtyWords = Objects.requireNonNull(lines)
                .collect(Collectors.toList());

        List<String> dirtyWordsLowerCase = dirtyWords.stream()
                .map(String::toLowerCase)
                .map(String::trim)
                .collect(Collectors.toList());

        return wordsToValidate.stream()
                .noneMatch(dirtyWordsLowerCase::contains);
    }
}