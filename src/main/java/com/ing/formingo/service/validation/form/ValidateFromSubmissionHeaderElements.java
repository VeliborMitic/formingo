package com.ing.formingo.service.validation.form;

import com.ing.formingo.service.FormService;
import com.ing.formingo.service.WebsiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ValidateFromSubmissionHeaderElements {
    private WebsiteService websiteService;
    private FormService formService;

    @Autowired
    public ValidateFromSubmissionHeaderElements(WebsiteService websiteService, FormService formService) {
        this.websiteService = websiteService;
        this.formService = formService;
    }

    public boolean validSiteUuid(String uuid) {
        return websiteService.getByUuidKey(uuid).isPresent();
    }

    public boolean validFormHtmlName(String name){
        return formService.getByHtmlName(name).isPresent();
    }
}