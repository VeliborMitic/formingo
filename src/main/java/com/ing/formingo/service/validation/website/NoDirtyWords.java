package com.ing.formingo.service.validation.website;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = NoDirtyWordsValidator.class)
@Documented
public @interface NoDirtyWords {

    String message() default "Dirty words are not allowed!";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}