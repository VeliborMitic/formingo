package com.ing.formingo.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

public interface BaseService<T> {

    List<T> get();

    List<T> get(Sort sort);

    Optional<Page<T>> get(Pageable pageable);

    Optional<T> get(Integer id);

    Optional<T> create(T entity);

    Optional<T> update(T entity);

    void delete(Integer id);

    void delete(T t);

    List<T> get(String search, Sort sort);

    Optional<Page<T>> get(String search, Pageable pageable);

    Optional<T> getByUuidKey(String uuid);

}