package com.ing.formingo.dto.website;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class WebsiteGetModel extends WebsitePostModel {

    private String prettyCreated;

    private String prettyUpdated;

    private Integer totalSubmissions;

    private Boolean isActive;
}