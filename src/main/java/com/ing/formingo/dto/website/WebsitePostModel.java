package com.ing.formingo.dto.website;

import com.ing.formingo.service.validation.website.NoDirtyWords;
import com.ing.formingo.service.validation.website.UniqueDomainName;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.UUID;

@Data
public class WebsitePostModel {

    private Integer id;

    private String uuidKey;

    @NotEmpty(message = "Please enter domain name!")
    @Size(min = 2, max = 50, message = "Size must be between 2 an 50 characters!")
    @NoDirtyWords
    private String domainName;

    @NotEmpty(message = "Please enter domain URL")
    @Size(min = 10, max = 50, message = "Size must be between 10 an 50 characters!")
    @URL(message = "Not valid URL!")
    @UniqueDomainName
    private String domain;

    public WebsitePostModel() {
        if (this.getUuidKey() == null)
            this.setUuidKey(UUID.randomUUID().toString());
    }

    public WebsitePostModel build(){
        return this;
    }
}