package com.ing.formingo.dto.form_submission;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.UUID;

@Data
public class FormSubmissionPostModel {

    private Integer id;

    private String uuidKey;

    @JsonIgnore
    @Size(min = 36, max = 36, message = "Site-key must be 36 characters!")
    private String siteUuidKey;

    @URL
    @JsonIgnore
    private String link;

    @JsonIgnore
    @Size(min = 2, max = 50, message = "Name size must be between 2 an 50 characters!")
    private String formHtmlName;

    @Email
    private String email;

    private String name;

    private String message;

    FormSubmissionPostModel() {
        if (this.getUuidKey() == null)
            this.setUuidKey(UUID.randomUUID().toString());
    }
}