package com.ing.formingo.dto.form_submission;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class FormSubmissionGetModel extends FormSubmissionPostModel {

    private String formDisplayName;

    private String prettyCreated;
}
