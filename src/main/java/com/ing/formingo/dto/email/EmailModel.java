package com.ing.formingo.dto.email;

import lombok.Data;

@Data
public class EmailModel {
    private String formHtmlName;
    private String siteUuidKey;
    private String websiteName;
    private String email;
    private String name;
    private String message;
    private String prettyCreated;
}