package com.ing.formingo.dto.user;

import com.ing.formingo.entity.Role;
import lombok.Data;

import java.time.Instant;

@Data
public class UserGetModel {
    private Integer id;
    private String uuidKey;
    private Instant created;
    private Instant updated;
    private String firstName;
    private String lastName;
    private String email;
    private Role role;
}