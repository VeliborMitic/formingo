package com.ing.formingo.dto.user;

import com.ing.formingo.entity.Role;
import lombok.Data;

@Data
public class LoggedUser {
    private Integer id;
    private String firstName;
    private String lastName;
    private String email;
    private String uuidKey;
    private Role role;
    private boolean active;
}