package com.ing.formingo.dto.slack;

import lombok.Data;

@Data
public class SlackMessageModel {
    private String formHtmlName;
    private String siteUuidKey;
    private String websiteName;
    private String email;
    private String name;
    private String message;
    private String prettyCreated;
    private String slackWebHook;
    private String slackName;
}
