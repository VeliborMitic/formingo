package com.ing.formingo.dto.form;

import lombok.Data;
import org.hibernate.validator.constraints.URL;

@Data
public class FormGetModel {

    private Integer id;

    private String htmlName;

    private String displayName;

    private String siteUuid;

    private String lastSubmission;

    private Integer totalSubmissions;

    private String created;

    @URL
    private String errorRedirectUrl;

    @URL
    private String successRedirectUrl;
}
