package com.ing.formingo.dto.notification;

import lombok.Data;

import javax.validation.constraints.Email;

@Data
public class NotificationPostModel {

    private Integer id;

    private String uuidKey;

    private Integer formId;

    @Email
    private String email;

    private String slackWebHook;
}
