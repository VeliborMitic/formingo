package com.ing.formingo.dto.notification;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class NotificationGetModel extends NotificationPostModel {

    private String created;

    private String updated;

    private Boolean isActive;
}
