package com.ing.formingo.response;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class ApiResponse {
    private final String redirectURL;
    private final String message;
    private final List<FieldError> errors;
    private final Map<Object, Object> data;
}