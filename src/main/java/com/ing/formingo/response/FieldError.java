package com.ing.formingo.response;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Setter(AccessLevel.NONE)
public class FieldError {
    private final String field;
    private final Object rejectedValue;
    private final String message;
}