package com.ing.formingo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FormInGoApplication {

    public static void main(String[] args) {
        SpringApplication.run(FormInGoApplication.class, args);
    }
}