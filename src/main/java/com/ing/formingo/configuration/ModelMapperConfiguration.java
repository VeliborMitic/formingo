package com.ing.formingo.configuration;

import com.ing.formingo.entity.Form;
import com.ing.formingo.entity.FormSubmission;
import com.ing.formingo.entity.Notification;
import com.ing.formingo.entity.Website;
import com.ing.formingo.dto.email.EmailModel;
import com.ing.formingo.dto.form.FormGetModel;
import com.ing.formingo.dto.form_submission.FormSubmissionGetModel;
import com.ing.formingo.dto.notification.NotificationGetModel;
import com.ing.formingo.dto.slack.SlackMessageModel;
import com.ing.formingo.dto.website.WebsiteGetModel;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.ocpsoft.prettytime.PrettyTime;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.Date;
import java.time.Instant;

@Configuration
public class ModelMapperConfiguration {

    @Bean
    public ModelMapper getModelMapper() {
        ModelMapper modelMapper = new ModelMapper();

        modelMapper.createTypeMap(FormSubmission.class, EmailModel.class)
                .addMappings(mapping -> mapping.using(instantToPrettyTimeConverter())
                    .map(FormSubmission::getCreated, EmailModel::setPrettyCreated));

        modelMapper.createTypeMap(FormSubmission.class, SlackMessageModel.class)
                .addMappings(mapping -> mapping.using(instantToPrettyTimeConverter())
                        .map(FormSubmission::getCreated, SlackMessageModel::setPrettyCreated));

        modelMapper.createTypeMap(Website.class, WebsiteGetModel.class)
                .addMappings(mapping -> mapping.using(instantToPrettyTimeConverter())
                        .map(Website::getCreated, WebsiteGetModel::setPrettyCreated))
                .addMappings(mapping -> mapping.using(instantToPrettyTimeConverter())
                        .map(Website::getUpdated, WebsiteGetModel::setPrettyUpdated));

        modelMapper.createTypeMap(FormSubmission.class, FormSubmissionGetModel.class)
                .addMappings(mapping -> mapping.using(instantToPrettyTimeConverter())
                        .map(FormSubmission::getCreated, FormSubmissionGetModel::setPrettyCreated));

        modelMapper.createTypeMap(Form.class, FormGetModel.class)
                .addMappings(mapping -> mapping.using(instantToPrettyTimeConverter())
                        .map(Form::getLastSubmission, FormGetModel::setLastSubmission))
                .addMappings(mapping -> mapping.using(instantToPrettyTimeConverter())
                        .map(Form::getCreated, FormGetModel::setCreated));

        modelMapper.createTypeMap(Notification.class, NotificationGetModel.class)
                .addMappings(mapping -> mapping.using(instantToPrettyTimeConverter())
                        .map(Notification::getCreated, NotificationGetModel::setCreated))
                .addMappings(mapping -> mapping.using(instantToPrettyTimeConverter())
                        .map(Notification::getUpdated, NotificationGetModel::setUpdated));

        return modelMapper;
    }

    private Converter<Instant, String> instantToPrettyTimeConverter(){
        return mappingContext -> {
            if (mappingContext.getSource() != null)
                return new PrettyTime().format(Date.from(mappingContext.getSource()));
            return null;
        };
    }
}
