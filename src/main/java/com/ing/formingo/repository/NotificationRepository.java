package com.ing.formingo.repository;

import com.ing.formingo.entity.Notification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface NotificationRepository extends BaseRepository<Notification> {

    Optional<Notification> findByUuidKey(String uuid);

    Optional<Notification> findByFormHtmlName(String name);

    List<Notification> findAllByFormHtmlName(String name);

    Optional<Page<Notification>> findAllByFormIdOrderByCreatedDesc(Integer id, Pageable pageable);
}