package com.ing.formingo.repository;

import com.ing.formingo.entity.FormSubmission;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FormSubmissionRepository extends BaseRepository<FormSubmission> {

    Optional<FormSubmission> findByUuidKey(String uuid);

    Optional<Page<FormSubmission>> findAllByForm_IdOrderByCreatedDesc(Integer formId, Pageable pageable);
}