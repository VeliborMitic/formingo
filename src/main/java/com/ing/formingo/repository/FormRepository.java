package com.ing.formingo.repository;

import com.ing.formingo.entity.Form;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FormRepository extends BaseRepository<Form> {

    Optional<Form> findByUuidKey(String uuid);

    Optional<Form> findByHtmlName(String name);

    Optional<Page<Form>> findAllByWebsiteUuidKey(String uuid, Pageable pageable);

    Optional<Page<Form>> findAllByWebsiteId(Integer id, Pageable pageable);

    List<Form> findAllByWebsiteId(Integer id);
}