package com.ing.formingo.repository;

import com.ing.formingo.entity.Website;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WebsiteRepository extends BaseRepository<Website> {

    Optional<Website> findByUuidKey(String uuid);

    Optional<Website> findByDomain(String domain);

    Boolean existsByDomain(String domain);

    @Query("SELECT (COUNT (w) > 0) FROM Website w WHERE (w.domain = :domain) AND w.id <> :myId")
    Boolean existsByDomainAndDifferentId(String domain, Integer myId);

    Optional<Page<Website>> findAllByUserUuidKey(String userUuid, Pageable pageable);

    Optional<Page<Website>> findAllByUserUuidKeyAndIsActiveTrueOrderByDomainNameAscDomainAsc(
            String userUuid, Pageable pageable);


}