package com.ing.formingo.repository;

import com.ing.formingo.entity.User;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends BaseRepository<User> {

    User findByEmail(String email);

    Optional<User> findByUuidKey(String uuid);
}