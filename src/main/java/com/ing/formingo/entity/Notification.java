package com.ing.formingo.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "notifications")
public class Notification extends BaseEntity{

    @Email
    private String email;

    private String slackWebHook;

    @ManyToOne
    @JoinColumn(name = "formHtmlName", referencedColumnName = "htmlName")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Form form;
}
