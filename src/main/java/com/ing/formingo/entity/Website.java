package com.ing.formingo.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "websites")
public class Website extends BaseEntity {

    private String domainName;

    private String domain;

    //By default, the JPA @ManyToOne and @OneToOne annotations are fetched EAGERly
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_uuid", referencedColumnName = "uuidKey", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;

    private int totalSubmissions;
}