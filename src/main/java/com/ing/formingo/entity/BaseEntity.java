package com.ing.formingo.entity;

import com.ing.formingo.entity.audit.TimeAudit;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.UUID;

@Data
@EqualsAndHashCode(callSuper = true)
@MappedSuperclass
public abstract class BaseEntity extends TimeAudit implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Size(min = 36, max = 36, message = "Site-key must be 36 characters!")
    private String uuidKey;

    private Boolean isActive;

    BaseEntity() {
        if (this.getUuidKey() == null)
            this.setUuidKey(UUID.randomUUID().toString());
        this.setIsActive(true);
    }
}