package com.ing.formingo.controller.api;

import com.ing.formingo.dto.email.EmailModel;
import com.ing.formingo.dto.form_submission.FormSubmissionPostModel;
import com.ing.formingo.dto.slack.SlackMessageModel;
import com.ing.formingo.entity.Form;
import com.ing.formingo.entity.FormSubmission;
import com.ing.formingo.response.ApiResponse;
import com.ing.formingo.response.FieldError;
import com.ing.formingo.service.FormService;
import com.ing.formingo.service.FormSubmissionService;
import com.ing.formingo.service.mail.MailClient;
import com.ing.formingo.service.slack.SlackNotificationService;
import com.ing.formingo.service.validation.form.ValidateFromSubmissionHeaderElements;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/form")
public class FormSubmissionApiController {
    private FormSubmissionService formSubmissionService;
    private FormService formService;
    private ModelMapper modelMapper;
    private ValidateFromSubmissionHeaderElements formHeaderValidator;
    private MailClient mailClient;
    private SlackNotificationService slackNotificationService;

    private static final String FAILED = "Submission failed!";
    private static final String SUCCESS = "Successfully submitted!";

    @Autowired
    public FormSubmissionApiController(FormSubmissionService formSubmissionService,
                                       FormService formService,
                                       ModelMapper modelMapper,
                                       ValidateFromSubmissionHeaderElements formHeaderValidator,
                                       MailClient mailClient,
                                       SlackNotificationService slackNotificationService) {
        this.formSubmissionService = formSubmissionService;
        this.formService = formService;
        this.modelMapper = modelMapper;
        this.formHeaderValidator = formHeaderValidator;
        this.mailClient = mailClient;
        this.slackNotificationService = slackNotificationService;
    }

    @GetMapping("/submissions")
    public List<FormSubmission> getAllSubmissions() {

        return formSubmissionService.get().stream()
                .filter(formSubmission -> formSubmission.getForm().getTotalSubmissions() > 10)
                .collect(Collectors.toList());
    }

    @GetMapping("/topform")
    public Form topForm(){
        return formService.get()
                .stream()
                .max(Comparator.comparing(Form::getTotalSubmissions))
                .orElseThrow(NoSuchElementException::new);
    }

    @GetMapping("/top3form")
    public List<Form> top5Forms(){
        return formService.get()
                .stream()
                .sorted(Comparator.comparing(Form::getTotalSubmissions).reversed())
                .limit(3)
                .collect(Collectors.toList());
    }


    @SuppressWarnings("OptionalGetWithoutIsPresent")
    @PostMapping
    public ApiResponse submit(HttpServletRequest request,
                              @Valid @RequestBody FormSubmissionPostModel formSubmissionPostModel,
                              BindingResult bindingResult) throws IOException {
        //Check if request contains header form-name
        if (request.getHeader("form-name") == null) {
            List<FieldError> fieldErrors = new ArrayList<>(Collections.singletonList(
                    new FieldError("formHtmlName",
                            request.getHeader("form-name"),
                            "Include \"form-name\" in request header!")));
            return new ApiResponse(null, FAILED, fieldErrors, null);
        }

        //Check if form name is registered in database
        if (!formHeaderValidator.validFormHtmlName(request.getHeader("form-name"))) {
            List<FieldError> fieldErrors = new ArrayList<>(Collections.singletonList(
                    new FieldError("formHtmlName",
                            request.getHeader("form-name"),
                            "Form name does not exist in database")));
            return new ApiResponse(formService.getByHtmlName(request.getHeader("form-name")).get().getErrorRedirectUrl(),
                    FAILED,
                    fieldErrors,
                    null);
        }

        formSubmissionPostModel.setFormHtmlName(request.getHeader("form-name"));

        //Check if request contains header site-key
        if (request.getHeader("site-key") == null) {
            List<FieldError> fieldErrors = new ArrayList<>(Collections.singletonList(
                    new FieldError("siteUuidKey",
                            request.getHeader("site-key"),
                            "Include \"site-key\" in request header!")));
            return new ApiResponse(formService.getByHtmlName(formSubmissionPostModel.getFormHtmlName()).get().getErrorRedirectUrl(),
                    FAILED,
                    fieldErrors,
                    null);
        }

        //Check if site-key is registered in database
        if (!formHeaderValidator.validSiteUuid(request.getHeader("site-key"))) {
            List<FieldError> fieldErrors = new ArrayList<>(Collections.singletonList(
                    new FieldError("siteUuidKey",
                            request.getHeader("site-key"),
                            "Error! Site key does not exist in database!")));
            return new ApiResponse(formService.getByHtmlName(formSubmissionPostModel.getFormHtmlName()).get().getErrorRedirectUrl(),
                    FAILED,
                    fieldErrors,
                    null);
        }

        if (bindingResult.hasFieldErrors()) {
            List<FieldError> fieldErrors = bindingResult.getFieldErrors().stream()
                    .map(fieldError -> new FieldError(fieldError.getField(),
                            fieldError.getRejectedValue(),
                            fieldError.getDefaultMessage()))
                    .collect(Collectors.toList());
            return new ApiResponse(
                    formService.getByHtmlName(formSubmissionPostModel.getFormHtmlName()).get().getErrorRedirectUrl(),
                    FAILED,
                    fieldErrors,
                    null);
        }

        FormSubmission mapped = modelMapper.map(formSubmissionPostModel, FormSubmission.class);
        mapped.setSiteUuidKey(request.getHeader("site-key"));
        mapped.setLink(request.getRemoteAddr());
        mapped.setForm(formService.getByHtmlName(formSubmissionPostModel.getFormHtmlName()).get());
        mapped.getForm().setTotalSubmissions(mapped.getForm().getTotalSubmissions() + 1);
        mapped.getForm().setLastSubmission(Instant.now());

        Optional<FormSubmission> optionalFormSubmission = formSubmissionService.create(mapped);

        if (optionalFormSubmission.isPresent()) {
//            TODO: increase totalSubmissions attribute
//            websiteService.getByUuidKey(
//                    mapped.getSiteUuidKey()).get().setTotalSubmissions(websiteService.getByUuidKey(mapped.getSiteUuidKey()).get().getTotalSubmissions() + 1);
            mailClient.prepareAndSend(
                    modelMapper.map(mapped, EmailModel.class));
            slackNotificationService.sendSlackNotification(
                    modelMapper.map(mapped, SlackMessageModel.class));

            return new ApiResponse(
                    formService.getByHtmlName(formSubmissionPostModel.getFormHtmlName()).get().getSuccessRedirectUrl(),
                    SUCCESS,
                    null,
                    null);
        }

        return new ApiResponse(formService.getByHtmlName(
                formSubmissionPostModel.getFormHtmlName()).get().getErrorRedirectUrl(),
                FAILED,
                null,
                null);
    }
}
