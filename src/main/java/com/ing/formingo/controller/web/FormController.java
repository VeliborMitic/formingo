package com.ing.formingo.controller.web;

import com.ing.formingo.entity.Form;
import com.ing.formingo.entity.Website;
import com.ing.formingo.dto.form.FormGetModel;
import com.ing.formingo.response.ApiResponse;
import com.ing.formingo.response.FieldError;
import com.ing.formingo.service.FormService;
import com.ing.formingo.service.WebsiteService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.constraints.Min;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/user/sites")
public class FormController {

    private FormService formService;
    private ModelMapper modelMapper;
    private WebsiteService websiteService;

    @Autowired
    public FormController(FormService formService, ModelMapper modelMapper, WebsiteService websiteService) {
        this.formService = formService;
        this.modelMapper = modelMapper;
        this.websiteService = websiteService;
    }

    @GetMapping("/{id}/forms")
    @SuppressWarnings({"unchecked"})
    public ModelAndView getAll(@PathVariable("id") @Min(1) Integer id, Pageable pageable) {

        ModelAndView mav = new ModelAndView("user/forms");
        Optional<Website> optionalWebsite = websiteService.get(id);

        //check if website with provided id is present, if yes, add site to view, if not redirect to error page
        //and add api response to view
        if (!optionalWebsite.isPresent()) {
            mav.setViewName("/error");
            return mav.addObject("errorResponse",  new ApiResponse(
                    "/error",
                    "Resource not found",
                    Collections.singletonList(new FieldError(
                            "id", id,
                            "Website with id " + id + " does'nt exist")),
                        null));
        }

        mav.addObject("site", optionalWebsite.get());

        Optional<Page<Form>> optionalPage =
                formService.getAllByWebsiteId(id, pageable);

        optionalPage.ifPresent(forms -> mav.addObject("totalCount", forms.getTotalElements()));

        return mav.addObject("siteForms", optionalPage.map
                (page -> {
                    List<FormGetModel> websites = page.stream()
                            .map(form -> modelMapper.map(form, FormGetModel.class))
                            .collect(Collectors.toList());
                    return new PageImpl(websites, pageable, page.getTotalElements());
                })
                .orElseGet(() -> new PageImpl(new ArrayList(), pageable, 0)));
    }

    @GetMapping("/forms/{id}/delete")
    public ModelAndView delete(@PathVariable("id") @Min(1) Integer id) {
        ModelAndView mav = new ModelAndView("redirect:/user/forms");

        Optional<Form> optionalForm = formService.get(id);
        if (!optionalForm.isPresent()) {
            mav.setViewName("/error");
            return mav.addObject("errorResponse",  new ApiResponse(
                    "/error",
                    "Resource not found",
                    Collections.singletonList(new FieldError(
                            "id", id,
                            "Form with id " + id + " does'nt exist")),
                    null));
        }
        formService.softDelete(id);

        return mav;
    }

}