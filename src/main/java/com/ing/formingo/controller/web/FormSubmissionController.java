package com.ing.formingo.controller.web;

import com.ing.formingo.entity.FormSubmission;
import com.ing.formingo.dto.form.FormGetModel;
import com.ing.formingo.dto.form_submission.FormSubmissionGetModel;
import com.ing.formingo.service.FormService;
import com.ing.formingo.service.FormSubmissionService;
import com.ing.formingo.service.WebsiteService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.constraints.Min;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/form")
public class FormSubmissionController {
    private FormSubmissionService formSubmissionService;
    private FormService formService;
    private WebsiteService websiteService;
    private ModelMapper modelMapper;

    @Autowired
    public FormSubmissionController(FormSubmissionService formSubmissionService,
                                    FormService formService,
                                    WebsiteService websiteService,
                                    ModelMapper modelMapper) {
        this.formService = formService;
        this.websiteService = websiteService;
        this.formSubmissionService = formSubmissionService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/{id}")
    public ModelAndView get(@PathVariable @Min(1) Integer id) {
        ModelAndView mav = new ModelAndView("form/submission");

        Optional<FormSubmission> optional = formSubmissionService.get(id);
        optional.ifPresent(
                fs ->
                        mav.addObject("formSubmission",
                                modelMapper.map(fs, FormSubmissionGetModel.class)));
        return mav;
    }

    @GetMapping("/{formId}/submissions")
    @SuppressWarnings({"unchecked"})
    public ModelAndView getAll(@PathVariable("formId") Integer formId, Pageable pageable) {

        ModelAndView mav = new ModelAndView("/user/submissions");
        Optional<Page<FormSubmission>> optionalPage =
                formSubmissionService.getAllByForm_IdOrderByCreatedDesc(formId, pageable);

        mav.addObject("totalCount", optionalPage.get().getTotalElements());
        mav.addObject("form", modelMapper.map(formService.get(formId).get(), FormGetModel.class));
        mav.addObject("website", formService.get(formId).get().getWebsite());

        return mav.addObject("submissions", optionalPage.map
                (page -> {
                    List<FormSubmissionGetModel> submissions = page.stream()
                            .map(s -> modelMapper.map(s, FormSubmissionGetModel.class))
                            .collect(Collectors.toList());
                    return new PageImpl(submissions, pageable, page.getTotalElements());
                })
                .orElse(new PageImpl(new ArrayList(), pageable, 0)));
    }

    @GetMapping("/{formId}/submissions/{id}/delete")
    public ModelAndView delete(@PathVariable("formId") @Min(1) Integer formId, @PathVariable("id") @Min(1) Integer id) {
        ModelAndView mav = new ModelAndView("redirect:/form/" + formId + "/submissions");

        Optional<FormSubmission> optionalSubmission = formSubmissionService.get(id);
        if (!optionalSubmission.isPresent()) {
            mav.addObject("error", "Resource not found");
        }
        formSubmissionService.delete(id);
        formService.get(formId).get().setTotalSubmissions(formService.get(formId).get().getTotalSubmissions() - 1);

        return mav;
    }
}
