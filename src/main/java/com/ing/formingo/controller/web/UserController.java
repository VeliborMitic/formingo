package com.ing.formingo.controller.web;

import com.ing.formingo.entity.User;
import com.ing.formingo.entity.Website;
import com.ing.formingo.dto.user.UserGetModel;
import com.ing.formingo.dto.website.WebsiteGetModel;
import com.ing.formingo.dto.website.WebsitePostModel;
import com.ing.formingo.response.ApiResponse;
import com.ing.formingo.response.FieldError;
import com.ing.formingo.service.FormService;
import com.ing.formingo.service.UserService;
import com.ing.formingo.service.WebsiteService;
import com.ing.formingo.service.security.AuthService;
import com.ing.formingo.service.validation.website.NoDirtyWords;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/user")
public class UserController {
    private AuthService authService;
    private UserService userService;
    private WebsiteService websiteService;
    private ModelMapper modelMapper;
    private FormService formService;

    @Autowired
    public UserController(AuthService authService,
                          UserService userService,
                          WebsiteService websiteService,
                          ModelMapper modelMapper,
                          FormService formService) {
        this.authService = authService;
        this.userService = userService;
        this.websiteService = websiteService;
        this.modelMapper = modelMapper;
        this.formService = formService;
    }

    @GetMapping("/home")
    public ModelAndView showUser() {
        ModelAndView mav = new ModelAndView("user/home");
        return mav.addObject("user",
                modelMapper.map(
                        userService.getByUuidKey(authService.getLoggedUser().getUuidKey()).get(),
                        UserGetModel.class));
    }

    @GetMapping("/sites")
    @SuppressWarnings({"unchecked"})
    public ModelAndView getAll(Pageable pageable) {

        ModelAndView mav = new ModelAndView("user/sites");
        Optional<Page<Website>> optionalPage =
                websiteService.getAllByUserUuidKeyAndIsActiveTrue(
                        authService.getLoggedUser().getUuidKey(), pageable);

        mav.addObject("totalCount", optionalPage.get().getTotalElements());
        mav.addObject("user",
                modelMapper.map(
                        userService.getByUuidKey(authService.getLoggedUser().getUuidKey()).get(),
                        UserGetModel.class));
        return mav.addObject("userSites", optionalPage.map
                (page -> {
                    List<WebsiteGetModel> websites = page.stream()
                            .map(w -> modelMapper.map(w, WebsiteGetModel.class))
                            .collect(Collectors.toList());
                    return new PageImpl(websites, pageable, page.getTotalElements());
                })
                .orElse(new PageImpl(new ArrayList(), pageable, 0)));
    }

    @GetMapping("/websiteForm")
    public ModelAndView addWebsite() {
        ModelAndView mav = new ModelAndView("user/websiteForm");
        WebsitePostModel websitePostModel = new WebsitePostModel();
        return mav.addObject("website", websitePostModel);
    }

    @GetMapping("/websiteForm/{id}")
    public ModelAndView updateWebsite(@PathVariable("id") @Min(1) Integer id) {
        ModelAndView mav = new ModelAndView("user/websiteForm");

        Optional<Website> optionalWebsite = websiteService.get(id);

        return optionalWebsite.map(website -> mav.addObject("website", modelMapper.map(
                website, WebsitePostModel.class))).orElse(null);
    }

    @PostMapping("/saveWebsite")
    @NoDirtyWords
    public ModelAndView saveOrUpdateWebsite(
            @Valid @ModelAttribute("website") WebsitePostModel websitePostModel,
            BindingResult bindingResult) {
        ModelAndView mav = new ModelAndView("user/websiteForm");

        if (bindingResult.hasFieldErrors()) {
            List<FieldError> fieldErrors = bindingResult.getFieldErrors().stream()
                    .map(fieldError ->
                            new FieldError(fieldError.getField(),
                                    fieldError.getRejectedValue(),
                                    fieldError.getDefaultMessage()))
                    .collect(Collectors.toList());
            return mav.addObject("error", fieldErrors);
        }

        //ADD NEW site code block
        if (websitePostModel.getId() == null) {
            Website website = modelMapper.map(websitePostModel, Website.class);
            website.setUser(modelMapper.map(authService.getLoggedUser(), User.class));

            Optional<Website> optionalWebsite = websiteService.create(website);
            mav.addObject("successMessage", "Successfully added!");

            return mav.addObject("website", optionalWebsite.map(
                    w -> modelMapper.map(w, WebsiteGetModel.class)
            ));
        }

        //UPDATE site code block
        Optional<Website> optionalWebsite = websiteService.get(websitePostModel.getId());
        if (!optionalWebsite.isPresent()) {
            return mav.addObject("error", "Resource not found");
        }

        Website website = modelMapper.map(optionalWebsite.get(), Website.class);
        website.setDomainName(websitePostModel.getDomainName());
        website.setDomain(websitePostModel.getDomain());

        Optional<Website> optionalUpdatedWebsite = websiteService.update(website);
        mav.addObject("successMessage", "Successfully updated!");

        return mav.addObject("website",
                optionalUpdatedWebsite.map(w -> modelMapper.map(w, WebsiteGetModel.class)
                ));
    }

    @GetMapping("/sites/{id}/delete")
    public ModelAndView delete(@PathVariable("id") @Min(1) Integer id) {
        ModelAndView mav = new ModelAndView("redirect:/user/sites");

        Optional<Website> optionalWebsite = websiteService.get(id);
        if (!optionalWebsite.isPresent()) {
            mav.setViewName("/error");
            return mav.addObject("errorResponse", new ApiResponse(
                    "/error",
                    "Resource not found",
                    Collections.singletonList(new FieldError(
                            "id", id,
                            "Website with id " + id + " does'nt exist")),
                    null));
        }

        websiteService.softDelete(id);
        formService.softDeleteAllBySiteId(id);

        return mav;
    }
}