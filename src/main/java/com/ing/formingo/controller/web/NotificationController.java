package com.ing.formingo.controller.web;

import com.ing.formingo.entity.Form;
import com.ing.formingo.entity.Notification;
import com.ing.formingo.dto.notification.NotificationGetModel;
import com.ing.formingo.dto.notification.NotificationPostModel;
import com.ing.formingo.response.ApiResponse;
import com.ing.formingo.response.FieldError;
import com.ing.formingo.service.FormService;
import com.ing.formingo.service.NotificationService;
import com.ing.formingo.service.validation.website.NoDirtyWords;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/notification")
public class NotificationController {

    private FormService formService;
    private NotificationService notificationService;
    private ModelMapper modelMapper;

    @Autowired
    public NotificationController(FormService formService, NotificationService notificationService, ModelMapper modelMapper) {
        this.formService = formService;
        this.notificationService = notificationService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/{formId}/configure")
    @SuppressWarnings({"unchecked"})
    public ModelAndView getAll(@PathVariable("formId") @Min(1) Integer id, Pageable pageable) {

        ModelAndView mav = new ModelAndView("user/notifications");

        Optional<Form> optionalForm = formService.get(id);

        //check if form with provided id is present, if yes, add form to view, if not - redirect to error page
        //and add api response to view
        if (!optionalForm.isPresent()) {
            mav.setViewName("/error");
            return mav.addObject("errorResponse", new ApiResponse(
                    "/error",
                    "Resource not found",
                    Collections.singletonList(new FieldError(
                            "id", id,
                            "Form with id " + id + " does'nt exist")),
                    null));
        }

        mav.addObject("form", optionalForm.get());

        Optional<Page<Notification>> optionalPage =
                notificationService.getAllByFormIdOrderByCreatedDesc(id, pageable);

        optionalPage.ifPresent(notifications -> mav.addObject("totalCount", notifications.getTotalElements()));

        return mav.addObject("notifications", optionalPage.map
                (page -> {
                    List<NotificationGetModel> notifications = page.stream()
                            .map(n -> modelMapper.map(n, NotificationGetModel.class))
                            .collect(Collectors.toList());
                    return new PageImpl(notifications, pageable, page.getTotalElements());
                })
                .orElseGet(() -> new PageImpl(new ArrayList(), pageable, 0)));
    }

    @GetMapping("/{id}/delete")
    public ModelAndView delete(@PathVariable("id") @Min(1) Integer id) {
        ModelAndView mav = new ModelAndView("redirect:/user/notifications");

        Optional<Notification> optionalNotification = notificationService.get(id);
        if (!optionalNotification.isPresent()) {
            mav.setViewName("/error");
            return mav.addObject("errorResponse", new ApiResponse(
                    "/error",
                    "Resource not found",
                    Collections.singletonList(new FieldError(
                            "id", id,
                            "Notification with id " + id + " does'nt exist")),
                    null));
        }

        notificationService.delete(id);
        return mav;
    }

    @GetMapping("/{formId}/notificationForm")
    public ModelAndView addNotification(@PathVariable("formId") @Min(1) Integer formId) {
        ModelAndView mav = new ModelAndView("user/notificationForm");
        NotificationPostModel notificationPostModel = new NotificationPostModel();
        mav.addObject("formId", formId);
        return mav.addObject("notification", notificationPostModel);
    }

    @GetMapping("/notificationForm/{id}")
    public ModelAndView updateNotification(@PathVariable("id") @Min(1) Integer id) {
        ModelAndView mav = new ModelAndView("user/notificationForm");
        Optional<Notification> optionalNotification = notificationService.get(id);

        return optionalNotification.map(notification -> mav.addObject("notification", modelMapper.map(
                notification, NotificationPostModel.class))).orElse(null);
    }

    @PostMapping("/saveNotification")
    @NoDirtyWords
    public ModelAndView saveOrUpdateNotification(@Valid @ModelAttribute("notification") NotificationPostModel notificationPostModel,
            BindingResult bindingResult) {

        ModelAndView mav = new ModelAndView("user/notificationForm");

        if (bindingResult.hasFieldErrors()) {
            List<FieldError> fieldErrors = bindingResult.getFieldErrors().stream()
                    .map(fieldError ->
                            new FieldError(fieldError.getField(),
                                    fieldError.getRejectedValue(),
                                    fieldError.getDefaultMessage()))
                    .collect(Collectors.toList());
            return mav.addObject("error", fieldErrors);
        }

        //ADD NEW notification code block
        if (notificationPostModel.getId() == null) {
            Notification notification = modelMapper.map(notificationPostModel, Notification.class);

            Optional<Notification> optionalNotification = notificationService.create(notification);
            mav.addObject("successMessage", "Successfully added!");

            return mav.addObject("notification", optionalNotification.map(
                    n -> modelMapper.map(n, NotificationPostModel.class)
            ));
        }

        //UPDATE notification code block
        Optional<Notification> optionalNotification = notificationService.get(notificationPostModel.getId());
        if (!optionalNotification.isPresent()) {
            return mav.addObject("error", "Resource not found");
        }

        Notification notification = modelMapper.map(notificationPostModel, Notification.class);
        notification.setForm(optionalNotification.get().getForm());

        Optional<Notification> optionalUpdatedNotification = notificationService.update(notification);
        mav.addObject("successMessage", "Successfully updated!");

        return mav.addObject("notification",
                optionalUpdatedNotification.map(n -> modelMapper.map(n, NotificationGetModel.class)
                ));
    }
}