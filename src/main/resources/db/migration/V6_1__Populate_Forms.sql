INSERT INTO forms(id,
                  uuid_key,
                  is_active,
                  display_name,
                  html_name,
                  error_redirect_url,
                  success_redirect_url,
                  total_submissions,
                  site_uuid)
VALUES
(2, '01f733a7-58f0-44e9-be2f-855d714a19c6', true, 'Contact Form', 'contact-form', 'http://www.mysite.com/error.html', 'http://mysite.com/subscription.html', 11, '8f604922-ed41-4ea7-b03e-661f3b5e6118'),
(3, '01f733a7-58f0-44e9-be2f-855d714a19c6', true, 'Registration Form', 'registration-form', 'http://www.mysite.com/error.html', 'http://mysite.com/subscription.html', 77, '8f604922-ed41-4ea7-b03e-661f3b5e6118'),
(4, '01f733a7-58f0-44e9-be2f-855d714a19c6', true, 'Licence Form', 'licence-form', 'http://www.mysite.com/error.html', 'http://mysite.com/subscription.html', 155, '8f604922-ed41-4ea7-b03e-661f3b5e6118'),
(5, '01f733a7-58f0-44e9-be2f-855d714a19c6', true, 'Order Form', 'order-form', 'http://www.mysite.com/error.html', 'http://mysite.com/subscription.html', 255, '8f604922-ed41-4ea7-b03e-661f3b5e6118')
;