INSERT INTO notifications (id,
                    is_active,
                    uuid_key,
                    email,
                    slack_web_hook,
                    slack_name,
                    form_html_name
                    )
VALUES (1,
        true,
        '01f733a7-58f0-44e9-be2f-855d714a18b5',
        'velibor@castel.co.rs',
        'https://hooks.slack.com/services/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',
        'velibor',
        'newsletter-subscription'
        );