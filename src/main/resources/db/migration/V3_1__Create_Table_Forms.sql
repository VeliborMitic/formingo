CREATE TABLE forms
(
    id                      SERIAL NOT NULL CONSTRAINT forms_pkey PRIMARY KEY,
    created                 TIMESTAMP NOT NULL DEFAULT Now(),
    updated                 TIMESTAMP NOT NULL DEFAULT Now(),
    uuid_key                VARCHAR(36),
    is_active               BOOLEAN NOT NULL,
    display_name            VARCHAR(255),
    html_name               VARCHAR(255) UNIQUE,
    error_redirect_url      VARCHAR(255),
    success_redirect_url    VARCHAR(255),
    last_submission         TIMESTAMP NOT NULL DEFAULT Now(),
    total_submissions       INTEGER,
    site_uuid               VARCHAR(36) NOT NULL CONSTRAINT fk75ni5d1d5k97y3u23wm76v94y REFERENCES websites (uuid_key) ON DELETE CASCADE
);

