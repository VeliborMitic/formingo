create table notifications
(
    id                  SERIAL NOT NULL CONSTRAINT emails_pkey PRIMARY KEY,
    created             TIMESTAMP DEFAULT Now(),
    updated             TIMESTAMP DEFAULT Now(),
    is_active           BOOLEAN NOT NULL,
    uuid_key            VARCHAR(36) UNIQUE,
    email               VARCHAR(255),
    slack_web_hook        VARCHAR(255),
    slack_name           VARCHAR(255),
    form_html_name      VARCHAR(255) NOT NULL CONSTRAINT fk83j33oh8y69lunsmq2tl5le5y REFERENCES forms (html_name) ON DELETE CASCADE
);