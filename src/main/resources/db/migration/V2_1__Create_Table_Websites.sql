create table websites
(
    id                SERIAL NOT NULL CONSTRAINT websites_pkey PRIMARY KEY,
    created           TIMESTAMP NOT NULL DEFAULT Now(),
    updated           TIMESTAMP NOT NULL DEFAULT Now(),
    uuid_key          VARCHAR(36) CONSTRAINT uk_fnk237j3i2dflu04la78hhhgr UNIQUE,
    is_active         BOOLEAN NOT NULL,
    domain            VARCHAR(255),
    domain_name       VARCHAR(255),
    total_submissions INTEGER NOT NULL,
    user_uuid         VARCHAR(36) NOT NULL CONSTRAINT fk22lf64g1w0lt32p02563yob2v REFERENCES users (uuid_key) ON DELETE CASCADE
);
