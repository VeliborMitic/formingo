create table submissions
(
    id                  SERIAL NOT NULL CONSTRAINT submissions_pkey PRIMARY KEY,
    created             TIMESTAMP DEFAULT Now(),
    updated             TIMESTAMP DEFAULT Now(),
    is_active           BOOLEAN NOT NULL,
    uuid_key            VARCHAR(36) UNIQUE,
    site_uuid_key       VARCHAR(36),
    email               VARCHAR(255),
    link                VARCHAR(255),
    name                VARCHAR(255),
    message             VARCHAR(255),
    form_Html_Name      VARCHAR(255) NOT NULL CONSTRAINT fkfqj8ftpwsoulnitp7yor26cn5 REFERENCES forms (html_name) ON DELETE CASCADE
);