INSERT INTO users (
                   id,
                   uuid_key,
                   is_active,
                   first_name,
                   last_name,
                   email,
                   password,
                   role)
VALUES (1, '01f733a7-58f0-44e9-be2f-855d714a19c5', true, 'John', 'Doe', 'john.doe@anonimous.com', '$2a$10$Q3/YiZuTVRQuPgWf.Gbm2eCieydi3hOnOPKvD1vUSWp3Noc7aLwva', 'ROLE_USER'),
       (2, 'fc0a74ef-a4a3-4d51-9cab-6b3f791a9cfc', true, 'Jane', 'Doe', 'jane.doe@anonimous.com', '$2a$10$Q3/YiZuTVRQuPgWf.Gbm2eCieydi3hOnOPKvD1vUSWp3Noc7aLwva', 'ROLE_USER');