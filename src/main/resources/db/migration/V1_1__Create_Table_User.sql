create table users
(
    id         SERIAL NOT NULL CONSTRAINT users_pkey PRIMARY KEY,
    created    TIMESTAMP NOT NULL DEFAULT Now(),
    updated    TIMESTAMP NOT NULL DEFAULT Now(),
    uuid_key   VARCHAR(36) CONSTRAINT uk_fq9ykexcc478foo5o7jc2tt23 UNIQUE,
    is_active  BOOLEAN NOT NULL,
    email      VARCHAR(255) CONSTRAINT uk_6dotkott2kjsp8vw4d0m25fb7 UNIQUE,
    first_name VARCHAR(50),
    last_name  VARCHAR(50),
    password   VARCHAR(255),
    role       VARCHAR(255)
);